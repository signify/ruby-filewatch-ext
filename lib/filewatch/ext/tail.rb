require "filewatch/ext/progress_tail"
require "forwardable"

module FileWatch
	module Ext
		class Tail
			extend Forwardable

			def_delegators :@target, :tail, :logger=, :subscribe, :sincedb_record_uid, :sincedb_write, :quit, :close_file

			attr_writer :target

			def self.new_observing_progress(opts = {})
				new({}, ProgressTail.new(opts))
			end

		    def initialize(opts = {}, target = YieldingTail.new(opts))
		      	@target = target
		    end
		end
	end
end