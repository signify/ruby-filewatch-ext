require 'filewatch/observing_tail'

module FileWatch
  module Ext
    class ProgressTail < FileWatch::ObservingTail

      public
      def initialize(opts={})
        super
        @opts[:progress_write_interval] = 500 unless @opts.include?(:progress_write_interval)
        @progress_last_write = 0
      end

      private
      def observe_read_file(watched_file, listener)
        changed = false
        loop do
          begin
            data = watched_file.file_read(32768)
            changed = true

            watched_file.buffer_extract(data).each do |line|
              @sincedb[watched_file.inode] += (line.bytesize + @delimiter_byte_size)
              size = watched_file.filestat.size
              pos = @sincedb[watched_file.inode]

              now = Time.now.to_f
              delta = now - @progress_last_write
              if delta * 1000 >= @opts[:progress_write_interval] or size == pos
                listener.accept(line, size, pos)
                @progress_last_write = now
              else
                listener.accept(line, nil, nil)
              end
            end
            # watched_file bytes_read tracks the sincedb entry
            # see TODO in watch.rb
            watched_file.update_bytes_read(@sincedb[watched_file.inode])
          rescue EOFError
            listener.eof
            watched_file.unwatch if @opts[:eof_close]
            break
          rescue Errno::EWOULDBLOCK, Errno::EINTR
            listener.error
            break
          rescue => e
            @logger.debug? && @logger.debug("observe_read_file: general error reading #{watched_file.path} - error: #{e.inspect}")
            listener.error
            break
          end
        end

        if changed
          now = Time.now.to_i
          delta = now - @sincedb_last_write
          if delta >= @opts[:sincedb_write_interval]
            @logger.debug? && @logger.debug("writing sincedb (delta since last write = #{delta})")
            _sincedb_write
            @sincedb_last_write = now
          end
        end
      end # def _read_file

    end # module ProgressTail
  end # module Ext
end # module FileWatch