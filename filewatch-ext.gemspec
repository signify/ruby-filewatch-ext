Gem::Specification.new do |spec|
  files = []
  dirs = %w{lib}
  dirs.each do |dir|
    files += Dir["#{dir}/**/*"]
  end

  spec.name = "filewatch-ext"
  spec.version = "1.1.0"
  spec.summary = "filewatch extensions"
  spec.description = "Extensions for ruby-filewatcher"
  spec.files = files
  spec.require_paths << "lib"

  spec.authors = ["Signify"]
  spec.email = ["dietmar@signifydata.com"]
  spec.homepage = "https://bitbucket.org/signify/ruby-filewatch-ext"
  spec.license = 'MIT'

  spec.add_runtime_dependency 'filewatch', ['>= 0.8.1', '~> 0.8']

  spec.add_development_dependency "stud"
end

